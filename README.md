# radionix

Script com interface interativa para reproduzir estações de rádios.

## Dependencias

* [mpg123](https://mpg123.de/)
* [GNU Grep](https://www.gnu.org/software/grep/)
* [GNU Sed](https://www.gnu.org/software/sed/manual/sed.html)

## Documentação

* [Capturas de tela](https://codeberg.org/pekman/radionix/wiki/captura-de-tela)
* [Perguntas Frequentes](https://codeberg.org/pekman/radionix/wiki/Faq)
* [Instalação](https://codeberg.org/pekman/radionix/wiki/Instalando)
